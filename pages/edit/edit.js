// pages/edit/edit.js
// 1. 获取数据库引用
// const db = wx.cloud.database()
// 2. 构造查询语句
// collection 方法获取一个集合的引用
// where 方法传入一个对象，数据库返回集合中字段等于指定值的 JSON 文档。API 也支持高级的查询条件（比如大于、小于、in 等），具体见文档查看支持列表
// get 方法会触发网络请求，往数据库取数据
// db.collection('edit').where({
//   publishInfo: {
//     country: 'United States'
//   }
// }).get({
//   success: function(res) {
//   // 输出 [{ "title": "The Catcher in the Rye", ... }]
//   console.log(res)
//  }
// })


// db.collection('edit').add({
//     data: {
//         edit1: '',
//         due: new Date('20210613'),
//         tag: ["cloud", "database"],
//         success: function(res) {
//             console.log(res)
//         }
//     }
// })
// wx.cloud.callFunction(){
//获取应用实例
const app = getApp()
    //数据库初始化
const DB = wx.cloud.database().collection("request")
let fromt = ""
let tot = ""
let people = ""
let short = ""
let name = ""
let full = ""
let wechat_id = ""

Page({

    //获取用户输入的开始时间
    fromt(event) {
        fromt = event.detail.value
    },
    //获取用户输入的结束时间
    tot(event) {
        tot = event.detail.value
    },
    //获取用户输入的人数
    people(event) {
        people = event.detail.value
    },
    //获取用户输入的标题
    short(event) {
        short = event.detail.value
    },
    //获取用户输入的昵称
    name(event) {
        name = event.detail.value
    },
    full(event) {
        full = event.detail.value
    },
    wechat_id(event){
        wechat_id=event.detail.value
    },
    //添加数据
    post() {
        DB.add({
            data: {
                fromt: fromt,
                tot: tot,
                people: people,
                short: short,
                name: name,
                full: full,
                wechat_id: wechat_id
            },
            success(res) {
                console.log("添加成功", res.detail)
            },
            fail(res) {
                console.log("添加失败", res)
            }
        })
    },

    //查询数据
    // getData() {
    //     DB.get({
    //         success(res) {
    //             console.log("查询成功", res)
    //         },
    //         fail(res) {
    //             console.log("查询失败", res)
    //         }
    //     })
    // }
})