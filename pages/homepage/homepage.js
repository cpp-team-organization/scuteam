// pages/homepage/homepage.js
const DB = wx.cloud.database().collection("request")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[]

  },
  tolaunch(){
    wx.navigateTo({
      url: '../edit/edit',
    })
  },
  toindex(){
    wx.navigateTo({
      url: '../index/index',
    })
  },
  touser(){
    wx.navigateTo({
      url: '../index2/index2',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this= this
    DB.get({
      success:res=>{
        console.log(res.data)
        this.setData({
          list:res.data
        })
      }
    })
  },
  addData(){
    DB.add({
      data:{
        name:"coxhin",
        people: 2,
      },
      success(res)
      {
        console.log("增加成功",res)
      },
      fail(res)
      {
        console.log("增加失败",res)
      }
      
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var _this= this
    DB.get({
      success:res=>{
        console.log(res.data)
        this.setData({
          list:res.data
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})