// logs.js
const util = require('../../utils/util.js')

Page({
  data: {
    logs: []
  },
  goback(){
    wx.navigateBack({
      delta: 1, // 回退前 delta(默认为1) 页面
      success: function (res) {
          // success
      },
      fail: function () {
          // fail
      },
      complete: function () {
          // complete
      }
  })

  }
  ,
  onLoad() {
    this.setData({
      logs: (wx.getStorageSync('logs') || []).map(log => {
        return {
          date: util.formatTime(new Date(log)),
          timeStamp: log
        }
      })
    })
  }
})
