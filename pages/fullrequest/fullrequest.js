// pages/fullrequest/fullrequest.js
const DB = wx.cloud.database().collection("request")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    rid:'',
    name:'',
    short:'',
    full:'',
    people:'',
    fromt:'',
    tot:'',
    wechat_id:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var Rid=options.rid
    var Name=options.name
    var Short=options.short
    var Full=options.full
    var People=options.people
    var Fromt=options.fromt
    var Tot=options.tot
    var Wechat_id=options.wechat_id
    this.setData({
      rid:Rid,
      name:Name,
      short: Short,
      full:Full,
      people:People,
      fromt:Fromt,
      tot:Tot,
      wechat_id:Wechat_id
    })
  //   DB.doc(Rid).get()({
  //     success(res){
  //       this.setData({
  //         name:res.data.name,
  //         short:res.data.short,
  //         full:res.data.full
  //       })
  //     }
  // })
},

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})